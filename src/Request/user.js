import axios from 'axios';
//import offlineUserList from "../Assets/randomPeople";
import offlineUserList from "../Assets/randomUser";

const userGetConnector = axios.create({
    method: 'get'
});

export const getUser = (number) => {
    //FIXME CORS Issue with randomuser (see https://github.com/RandomAPI/Randomuser.me-Node/issues/170)
    /*
    let url = "https://randomuser.me/api/";

    if(number && number > 0){
        url = url.concat("?results=", numr);
    }
         return userGetConnector.get(url)
            .then(response => {
                return response.data.results;
            }).catch(err => console.log(err));
    */

    //FIX to use offline dataset from uinames (use url.concat("?amount=", number, '&ext')) or reandomuser
    return new Promise(function(resolve) {
        setTimeout(function() {
            resolve({data: offlineUserList.slice(0, number)});
        }, 300);
    });
};

export const getListAsso = () => {
    let url = "http://413fa2f2.ngrok.io/api/v1/directory/80/";

    return userGetConnector.get(url)
        .then(response => {
            return response;
        }).catch(err => console.log(err));
};