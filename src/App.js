import React from 'react';
import './App.css';
import {Realisation1} from "./Component/Realisation1";
import {Realisation2} from "./Component/Realisation2";
import {Realisation3} from "./Component/Realisation3";
import Divider from "@material-ui/core/Divider";
import { Route, Switch, BrowserRouter as Router, Redirect } from 'react-router-dom';
import {GenerationForm} from "./Component/GenerationForm";

function App() {
    return(
        <React.Fragment>
            <Realisation1 text={"Test pour test"}/>
            <br/>
            <Divider/>
            <br/>
            <Realisation2/>
            <br/>
            <Divider/>
            <Router basename={process.env.PUBLIC_URL}>
                <Switch>
                    <Route exact path="/" render={(props) => <GenerationForm {...props}/>} />
                    <Route exact path='/view/:number' render={(props) => <Realisation3 {...props}/>} />
                    <Route exact path='/view/' render={() => <Redirect to="/"/>} />
                </Switch>
            </Router>
        </React.Fragment>
    );
}

export default App;
