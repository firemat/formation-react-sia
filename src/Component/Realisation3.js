import React from 'react';
import CardComponent from "./Card";
import {Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import {Search as SearchIcon} from '@material-ui/icons';
import Input from "@material-ui/core/Input";
import {getUser} from "../Request/user";
import DialogComponent from "./DialogComponent";

export function Realisation3(props){
    const [baseUser, setBaseUser] = React.useState([]);
    const [currentBaseUser, setCurrentBaseUser] = React.useState([]);
    const [firstLoad, setFirstLoad] = React.useState(true);
    const [openDialog, setOpenDialog] = React.useState(false);
    /* Fix to use uinames dataset
        const [personDialog, setPersonDialog] = React.useState({name: "", surname: "", photo: "", title: "C ",
        gender: "female", birthday: {dmy: ""}, email: "", age: 0});
     */
    const [personDialog, setPersonDialog] = React.useState(
        {"gender":"female","name":{"title":"Miss","first":"Edith","last":"Pfeiffer"},"email":"edith.pfeiffer@example.com","dob":{"date":"1983-07-27T23:35:02.111Z","age":37},"phone":"0981-8823838","picture":{"thumbnail":"https://randomuser.me/api/portraits/thumb/women/80.jpg"}}
    );

    const handleSearch = (event) => {
        const value= event.target.value.toLowerCase();
        if(value.length > 0){
            setCurrentBaseUser(baseUser.filter((people) => {
                const name = people.name.first.toLowerCase() + " " + people.name.last.toLowerCase();
                return name.includes(value);
            }));
        }else{
            setCurrentBaseUser(baseUser);
        }
    };

    if(props.match.params.number > 0 && firstLoad){
        setFirstLoad(false);
        getUser(props.match.params.number).then((response) => {
            setBaseUser(response.data);
            setCurrentBaseUser(response.data);
        }).catch((err) => {console.error(err);});
    }

    const clickHandler = (index) => {
        setOpenDialog(true);
        console.log(currentBaseUser[index]);
        setPersonDialog(currentBaseUser[index]);
    };

    const closeHandler = () => {
        setOpenDialog(false);
    };
    
    return(
        <React.Fragment>
            <Grid container direction="row" justify="center" alignItems="center">
                <Grid item md={5}/>
                <Grid item md={2}>
                    <Button variant="contained" color="primary" component={Link} to={'/'}>Go Back</Button>
                </Grid>
                <Grid item xs={3} md={3} lg={3}>
                    <Input id="people-search" type="search"
                        startAdornment={<InputAdornment position="start"><SearchIcon/></InputAdornment>}
                        placeholder="Search people here"
                        onChange={handleSearch}
                        style={{minWidth:"16vw", paddingLeft:0}}/>
                </Grid>
                <Grid item md={5}/>
            </Grid>
            <br/>
            <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                {props.match.params.number==="0" ? <Grid item md={3}><Typography component={"h2"}>Nothing to display here</Typography></Grid> : null}
                {currentBaseUser.map((element, index) => {
                    return (
                        <Grid item md={3} key={ 'grid_' + index}>
                            <CardComponent title={element.name.first + " " + element.name.last}
                                key={element.name.last + '_' + index}
                                address={element.email}
                                personIndex={index}
                                clickHandler={clickHandler}
                                img={element.picture.thumbnail}
                                imgAlt={"image"}
                            />
                        </Grid>);
                })}
                <DialogComponent closingHandler={closeHandler}
                    fullWidth={true} open={openDialog}
                    breakPointWidth={"lg"} person={personDialog}/>
            </Grid>
        </React.Fragment>
    );
}