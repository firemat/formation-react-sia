import React from 'react';
import {TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";

export function GenerationForm(){
    const [fieldValue, setFieldValue] = React.useState("");

    const handleChange = (event) => {
        setFieldValue(event.target.value);
    };

    return(
        <React.Fragment>
            <Grid container direction="row" justify="center" alignItems="center">
                <Grid item md={3}/>
                <Grid item md={4}>
                    <TextField id="standard-name" label="People to generate" value={fieldValue} onChange={handleChange}/>
                </Grid>
                <Grid item md={1}>
                    <Button variant="contained" color="primary" component={Link} to={'/view/'+fieldValue}>Generate</Button>
                </Grid>
                <Grid item md={4}/>
            </Grid>
        </React.Fragment>
    );
}