import React from 'react';
import * as PropTypes from "prop-types";

import {Button, Card, CardActions, CardContent,
    CardMedia, Typography, createStyles, makeStyles } from '@material-ui/core';

CardComponent.propTypes = {
    address: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired,
    img: PropTypes.string.isRequired,
    imgAlt: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

const useStyles = makeStyles(
    createStyles(theme => ({
        card: {
            maxWidth: '20vw',
            minHeight: '25vh',
            minWidth: '4vw'
        },
        img_card: {
            padding: '2.5vw',
            minHeight: '2.5vw',
            minWidth: '2.5vw'
        },
        button: {
            '&:hover': {
                color: theme.palette.secondary.dark,
            },
        },
        area: {
            minHeight: '48vh'
        },
        bigAvatar: {
            margin: "auto",
            marginTop: "10px",
            borderRadius: "50%",
            width: 120,
            height: 120,
        },
    }),
    )
);

function CardComponent(props) {
    const classes = useStyles();
    const openDetail = () => {
        props.clickHandler(props.personIndex);
    };

    return (
        <Card className={classes.card} elevation={5}>
            <CardMedia alt={props.imgAlt} className={classes.bigAvatar}
                image={props.img} max-height="120" title={props.title}/>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {props.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {props.address}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" component={"p"} className={classes.button} onClick={openDetail}>
                    See more
                </Button>
            </CardActions>
        </Card>
    );
}

export default CardComponent;
