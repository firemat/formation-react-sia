import React from 'react';
import * as PropTypes from "prop-types";
import {Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {getListAsso} from "../Request/user";

Exemple_hook.propTypes = {
    text: PropTypes.string.isRequired
};

export function Exemple_hook({text}){
    const info = () => {
        getListAsso().then(data => {
            console.info(data);
            return 'OK';
        }).catch(err => {
            console.error(err);
            return 'KO';
        });
    };

    return(
        <React.Fragment>
            <Button onClick={info}>Test</Button>
            <Typography component={"p"}>Je suis un hook</Typography>
            <Typography component={"p"}>{text ? text : "Mais je n'ai rien à afficher"}</Typography>
        </React.Fragment>
    );
}