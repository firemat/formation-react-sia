import React from 'react';
import * as PropTypes from "prop-types";

import {Button, createStyles, Dialog, DialogActions, DialogContent,
    DialogTitle, Typography, Grid, Avatar, makeStyles} from '@material-ui/core';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PhoneIcon from '@material-ui/icons/Phone';
import WcIcon from '@material-ui/icons/Wc';
import IconButton from "@material-ui/core/IconButton";
import CakeIcon from '@material-ui/icons/Cake';

DialogComponent.propTypes = {
    open: PropTypes.bool.isRequired,
    closingHandler: PropTypes.func.isRequired,
    breakPointWidth: PropTypes.string.isRequired,
    fullWidth: PropTypes.bool.isRequired,
    overrideContent: PropTypes.bool,
};

const useStyles = makeStyles(
    createStyles(theme => ({
        bigAvatar: {
            margin: "auto",
            width: 240,
            height: 240,
        },
        button: {
            margin: theme.spacing(1),
        },
    }),
    )
);

export default function DialogComponent({person, open, closingHandler, breakPointWidth, fullWidth, overrideContent, ...props}) {
    const classes = useStyles();
    return (
        <Dialog aria-labelledby="Dialog-Content-title" fullWidth={fullWidth} maxWidth={breakPointWidth}
            onClose={closingHandler} open={open} scroll={'paper'}>
            <DialogTitle id="Dialog-Content-title">{person.name.title.charAt(0).toUpperCase() + person.name.title.slice(1) +". "+person.name.first + " " + person.name.last}</DialogTitle>
            {(overrideContent !== undefined) && (overrideContent === true)
                ? props.children
                : <React.Fragment>
                    <DialogContent dividers={true}>
                        <Grid container direction="row" justify="space-evenly" alignItems="center">
                            <Grid container item xs={5}>
                                <Avatar alt={person.name.first + " " + person.name.last} src={person.picture.thumbnail} className={classes.bigAvatar}/>
                            </Grid>
                            <Grid container item xs={5} direction="column">
                                <Grid item md={12}>
                                    <IconButton className={classes.button} aria-label="place" color="primary" component={"button"}>
                                        <WcIcon/>
                                    </IconButton>
                                    <Typography component={"span"}>{person.gender.charAt(0).toUpperCase() + person.gender.slice(1)}</Typography>
                                </Grid>
                                <Grid item md={12}>
                                    <IconButton className={classes.button} aria-label="place" color="primary" component={"button"}>
                                        <CakeIcon/>
                                    </IconButton>
                                    <Typography component={"span"}>{new Date(person.dob.date).toLocaleDateString()} ({person.dob.age} years old)</Typography>
                                </Grid>
                                <Grid item md={12}>
                                    <IconButton className={classes.button} aria-label="place" color="primary" component={"a"} href={"mailto:"+person.email}>
                                        <MailOutlineIcon/>
                                    </IconButton>
                                    <Typography component={"span"}>{person.email}</Typography>
                                </Grid>
                                <Grid item md={12}>
                                    <IconButton className={classes.button} aria-label="place" color="primary" component={"a"} href={"tel:"+person.phone}>
                                        <PhoneIcon/>
                                    </IconButton>
                                    <Typography component={"span"}>{person.phone}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={closingHandler} color="primary">Close</Button>
                    </DialogActions>
                </React.Fragment>
            }

        </Dialog>
    );
}
