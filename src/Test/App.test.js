import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

import Enzyme, {shallow} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {Exemple_hook} from "../Component/Exemple_hook";
import Exemple_classe from "../Component/Exemple_classe";
import {Typography} from "@material-ui/core";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the render of global view', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it.skip('have hook component', () => {
        const view = shallow(<App/>);
        expect(view.exists(Exemple_hook)).toBe(true);
    });

    it.skip('have class component', () => {
        const view = shallow(<App/>);
        expect(view.exists(Exemple_classe)).toBe(true);
    });
});

describe('Examining the display for text attribute', () => {
    it.skip('have hook correct comportment with text', () => {
        const withText = shallow(<Exemple_hook text={"Test"}/>);
        const withoutText = shallow(<Exemple_hook/>);
        expect(withText.find(Typography).last().text()).toContain("Test");
        expect(withoutText.find(Typography).last().text()).toContain("rien");
    });

    it.skip('have class correct comportment with text', () => {
        const withText = shallow(<Exemple_classe text={"Test"}/>);
        const withoutText = shallow(<Exemple_classe/>);
        expect(withText.find(Typography).last().text()).toContain("Test");
        expect(withoutText.find(Typography).last().text()).toContain("rien");
    });
});